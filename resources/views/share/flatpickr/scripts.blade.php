<script src="https://npmcdn.com/flatpickr/dist/flatpickr.min.js"></script><!-- flatpickr(ライブラリ)のスクリプト -->
<script src="https://npmcdn.com/flatpickr/dist/l10n/ja.js"></script><!-- 日本語化のためのスクリプト -->
<script>
  flatpickr(document.getElementById('due_date'),{
    locale:'ja',
    dateFormat:"Y/m/d",
    minDate:new Date()
  });
</script>