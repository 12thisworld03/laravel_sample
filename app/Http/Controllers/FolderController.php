<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFolder;
use Illuminate\Http\Request;    //クラスのインポート
use App\Folder;
use Illuminate\Support\Facades\Auth;

class FolderController extends Controller
{
    public function showCreateForm(){
        return view('folders/create');
    }

    public function create(CreateFolder $request){
        $folder = new Folder();   //フォルダモデルのインスタンスを作成する
        $folder->title = $request->title;   //タイトルに入力値を代入する
        Auth::user()->folders()->save($folder);    //インスタンスの状態をデータベースに書き込む,ユーザーに紐づけて保存

        return redirect()->route('tasks.index',[
            'folder' => $folder->id,
        ]);
    }

    public function showEditForm(int $id,int $task_id){
        $task = Task::find($task_id);

        return view('tasks/edit',[
            'task' => $task,
        ]);
    }
    
}
